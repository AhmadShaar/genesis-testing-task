$(window).on('load', function() {

    /**
     * getInfo is a Recursion function which will be called for each lead row
     */
    getInfo($('.lead:first'));

    $('#leadModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var leadId = button.data('value') // Extract info from data-* attributes
        let lead = JSON.parse(localStorage.getItem(leadId));// getting the lead from local storage
        for (var attribute in lead) { // mapping between the lead attributes and appropriate table cell
            $('#leadModal table').find(`.${attribute}`).html(lead[attribute]);
        }
        $('#leadModal  #leadModalLabel').text(lead.full_name); // change the model title
    })

})


function getInfo(leadRow){

    // get lead id (stored in custom attribute called "data-value"
    let leadId = leadRow.data('value');

    // storing the next lead row we are going to check this variable to stop this recursion function
    let nextRow = leadRow.next();

    // if the current lead isn't stored local storage,
    // we have to make an ajax request to get its info,
    // when the request is succeeded, we have to store the lead in local store
    // and repeat calling this function again
    if(!isLeadExistsinStorage(leadId)){
        $.ajax({
            method:'GET',
            url:`/getInfo/${leadId}`,
            success(e) {
                insertToLocalStorage(e);
                getFromLocalStorage(e.id);
                if(nextRow.length != 0)
                    getInfo(leadRow.next());
            },
            error(e){
                console.log("ERROR");
            }
        });
    }else{ // if the current lead is stored in local storage, we have to fetch its info,
           // and repeat calling this function again
        getFromLocalStorage(leadId);
        if(nextRow.length != 0)
            getInfo(leadRow.next());
    }


}

// check if the current lead is stored in local storage
function isLeadExistsinStorage(leadId){
    return (localStorage.getItem(leadId) != null)
}

// storing the lead in local storage with key lead.id
function insertToLocalStorage(lead){
    localStorage.setItem(lead.id,  JSON.stringify(lead));
}

// fetch the lead from local storage
function getFromLocalStorage(leadId){
    let lead = localStorage.getItem(leadId)
    modifyView(lead)
}

//show lead info
function modifyView(lead){
    lead = JSON.parse(lead);
    $(`.lead-${lead.id}`).find('.phone-number').html(lead.phone_number);
    $(`.lead-${lead.id}`).find('.email').html(lead.email);
    $(`.lead-${lead.id}`).find('.show').removeAttr('disabled').toggleClass('btn-secondary').toggleClass('btn-success');
    $(`.lead-${lead.id}`).find('.show i').toggleClass('fa-eye-slash').toggleClass('fa-eye');

}
