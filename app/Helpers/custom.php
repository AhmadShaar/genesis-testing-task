<?php
use GuzzleHttp\Client;

/**
 * @param $url string URL
 * @return The body of the received response
 * @throws \GuzzleHttp\Exception\GuzzleException
 */
function callApi($url){

    $client  = new Client();
    $response = $client->get($url);
    $body = json_decode($response->getBody()->getContents());
    return $body->result;

}
