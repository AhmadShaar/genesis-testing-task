<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class LeadResource extends JsonResource
{
    protected $leadFields;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'                         => $this->ID,
            'full_name'                  => $this->NAME." ".$this->LAST_NAME,
            'phone_number'               => $this->PHONE[0]->VALUE,
            'email'                      => $this->EMAIL[0]->VALUE,
            'company'                    => (isset($this->COMPANY_TITLE))? $this->COMPANY_TITLE : "--",
            'birthdate'                  => Carbon::parse($this->BIRTHDATE)->format('Y-m-d'),
            'gender'                     => $this->mapValue('UF_CRM_1661669651915', $this->UF_CRM_1661669651915),
            'currency_id'                => $this->CURRENCY_ID,
            'status_id'                  => $this->STATUS_ID,
            'education_qualification'    =>  $this->mapValue('UF_CRM_1661669458993', $this->UF_CRM_1661669458993),//$this->UF_CRM_1661669458993,


        ];
    }
    public function __construct($resource)
    {
        parent::__construct($resource);
        $this->leadFields = session()->get('leadFields');
    }

    public function mapValue($attributeKey, $attrobuteValue){

        foreach ($this->leadFields->{$attributeKey}->items as $item):

            if($item->ID == $attrobuteValue)
                return $item->VALUE;
        endforeach;
        return "--";
    }
}
