<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Http\Resources\LeadResource;
use Symfony\Component\HttpFoundation\Response;

class HomeController extends Controller
{

    public function index(){
        // to avoid request the api everytime we refresh the page I put data in session variable
        if(!session()->has('leads')):
            // callApi is a custom helper exists ===> App\Helpers\custom.php
            $leads = callApi("https://b24-x3h9vq.bitrix24.com/rest/1/hv9kyeskz26ajur4/crm.lead.list.json");
            session()->put('leads', $leads);
        else:
            $leads = session()->get('leads');
        endif;

        return view('index', compact('leads'));

    }

    public function getInfo($leadId){
        try{
            $lead =  callApi("https://b24-x3h9vq.bitrix24.com/rest/1/hv9kyeskz26ajur4/crm.lead.get.json?ID=$leadId");
            return response()->json(new LeadResource($lead), Response::HTTP_OK) ;
        }
        catch (\Exception $ex){
            return response()->json("ERROR DURING CALL API", Response::HTTP_BAD_REQUEST) ;
        }

    }

    public function __construct()
    {
        if(!session()->has('leadFields')):
            $leadFields = callApi("https://b24-x3h9vq.bitrix24.com/rest/1/hv9kyeskz26ajur4/crm.lead.fields.json");
            session()->put('leadFields', $leadFields);
        endif;
    }

}
