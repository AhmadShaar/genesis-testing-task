<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lead extends Model
{
    use HasFactory;
    protected $guarded = [];


    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
//        $this->getcontactData();
    }

    public function getFullNameAttribute(){
        return $this->NAME.' '.$this->LAST_NAME;
    }

    public function getCompanyAttribute(){
        return (!isset($this->COMPANY_TITLE))?"--": $this->COMPANY_TITLE;
    }

    public function getcontactData(){

        $response =  callApi("https://b24-x3h9vq.bitrix24.com/rest/1/hv9kyeskz26ajur4/crm.lead.get.json?ID=$this->ID");
        $this->phoneNumber = $response->PHONE[0]->VALUE;
        $this->email = $response->EMAIL[0]->VALUE;

    }
//
//    public function getPhoneNumberAttribute(){
//
//        $response =  callApi("https://b24-x3h9vq.bitrix24.com/rest/1/hv9kyeskz26ajur4/crm.lead.get.json?ID=$this->ID");
//        return $response->PHONE[0]->VALUE;
//    }
//
//    public function getEmailAttribute(){
//
//        $response =  callApi("https://b24-x3h9vq.bitrix24.com/rest/1/hv9kyeskz26ajur4/crm.lead.get.json?ID=$this->ID");
//        return $response->EMAIL[0]->VALUE;
//
//    }
}
