<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css"
          integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.7.1/css/all.min.css"
          integrity="sha512-3M00D/rn8n+2ZVXBO9Hib0GKNpkm8MSUU/e2VNthDyBYxKWG+BftNYYcuEjXlyrSO637tidzMBXfE7sQm0INUg=="
          crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <title>Genesis Testing Task</title>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-10 offset-1">
                <table class="table table-striped">
                    <thead>
                        <th>ID</th>
                        <th>Full Name</th>
                        <th>Phone Number</th>
                        <th>Email</th>
                        <th>Company</th>
                        <th>Action</th>
                    </thead>
                    <tbody>
                    @foreach($leads as $lead)
{{--                        @php--}}
{{--                            $lead = new App\Models\Leed((array)$lead);--}}
{{--                        @endphp--}}
                        <tr class="lead lead-{{$lead->ID}}" data-value="{{$lead->ID}}">
                            <td>{{$lead->ID }}</td>
                            <td>{{$lead->NAME ." ".$lead->LAST_NAME}}</td>
                            <td class="phone-number">
                                <div class="loader1 info-loader">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </td>
                            <td class="email">
                                <div class="loader1 info-loader">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </td>
                            <td>{{(isset($lead->COMPANY_TITLE))? $lead->COMPANY_TITLE:"--"}}</td>
                            <td>
                                <button class="btn btn-secondary show"
                                        data-toggle="modal" data-target="#leadModal"
                                        data-value="{{$lead->ID}}" disabled="disabled">
                                    <i class="fas fa-eye-slash"></i>
                                </button>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="modal fade" id="leadModal" tabindex="-1" role="dialog" aria-labelledby="leadModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="leadModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                   <table class="table">
                       <tbody>
                       <tr>
                           <td>Full Name</td>
                           <td class="full_name"></td>
                       </tr>
                       <tr>
                           <td>Phone Number</td>
                           <td class="phone_number"></td>
                       </tr>
                       <tr>
                           <td>Email</td>
                           <td class="email"></td>
                       </tr>
                       <tr>
                           <td>Gender</td>
                           <td class="gender"></td>
                       </tr>
                       <tr>
                           <td>Company</td>
                           <td class="company"></td>
                       </tr>
                       <tr>
                           <td>Birthdate</td>
                           <td class="birthdate"></td>
                       </tr>
                       <tr>
                           <td>Education Qualification</td>
                           <td class="education_qualification"></td>
                       </tr>
                       <tr>
                           <td>Currency</td>
                           <td class="currency_id"></td>
                       </tr>
                       <tr>
                           <td> status_id</td>
                           <td class="status_id"></td>
                       </tr>
                       </tbody>
                   </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>

    <script src="https://code.jquery.com/jquery-3.6.1.min.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct"
            crossorigin="anonymous"></script>

    <script src="{{asset('assets/js/genesis_script.js')}}"></script>

</body>
</html>
